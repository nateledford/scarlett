import json
import os


class Config:
    curr_config = None

    def __init__(self):
        if not os.path.exists('config.json'):
            Config.__create_config()

        Config.curr_config = self.load_config()
        self.ASSETS_DEBUG = Config.curr_config['ASSETS_DEBUG']
        self.DB_PATH = Config.curr_config['DB_PATH']
        self.DEBUG = Config.curr_config['DEBUG']
        self.DROPBOX_ACCESS_TOKEN = Config.curr_config['DROPBOX_ACCESS_TOKEN']
        self.PORT = Config.curr_config['PORT']
        self.SECRET_KEY = Config.curr_config['SECRET_KEY']
        self.STORAGE_CONTAINER = Config.curr_config['STORAGE_CONTAINER']
        self.STORAGE_PROVIDER = Config.curr_config['STORAGE_PROVIDER']
        self.STORAGE_SERVER_URL = Config.curr_config['STORAGE_SERVER_URL']
        self.VERIFY_SSL_CERT = Config.curr_config['VERIFY_SSL_CERT']

    def to_json(self):
        j = {
            'ASSETS_DEBUG': self.ASSETS_DEBUG,
            'DB_PATH': self.DB_PATH,
            'DEBUG': self.DEBUG,
            'DROPBOX_ACCESS_TOKEN': self.DROPBOX_ACCESS_TOKEN,
            'PORT': self.PORT,
            'SECRET_KEY': self.SECRET_KEY,
            'STORAGE_CONTAINER': self.STORAGE_CONTAINER,
            'STORAGE_PROVIDER': self.STORAGE_PROVIDER,
            'STORAGE_SERVER_URL': self.STORAGE_SERVER_URL,
            'VERIFY_SSL_CERT': self.VERIFY_SSL_CERT
        }
        return j

    @staticmethod
    def load_config():
        with open('config.json', 'r') as infile:
            data = json.load(infile)
            return data

    def save_config(self):
        with open('config.json', 'w') as outfile:
            json.dump(self.to_json(), outfile)

    @staticmethod
    def __create_config():
        j = {
            'ASSETS_DEBUG': False,
            'DB_PATH': 'images.db',
            'DROPBOX_ACCESS_TOKEN': '',
            'DEBUG': False,
            'PORT': 5000,
            'SECRET_KEY': 'Hey, I just met you...',
            'STORAGE_CONTAINER': '/',
            'STORAGE_PROVIDER': 'LOCAL',
            'STORAGE_SERVER_URL': '/files',
            'VERIFY_SSL_CERT': True
        }
        with open('config.json', 'w') as outfile:
            json.dump(j, outfile)

config = Config()
config = config.curr_config
