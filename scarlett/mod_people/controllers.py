import json
import math
import re

import requests
from flask import Blueprint, render_template, request
from sqlalchemy import text

from scarlett.data import session
from scarlett.data.models.Images import Image
from scarlett.data.models.People import Person
from scarlett.data.stats import Stats

people = Blueprint('people', __name__)


@people.route('/', methods=['GET'])
def index():
    page = request.args.get('page')
    person = request.args.get('person')
    num_items = 20

    if page is None:
        page = 1
    else:
        page = int(page)

    if person is None or person == 'None':
        query = 'SELECT * FROM person ORDER BY Name LIMIT {0} OFFSET {1}'.format(num_items, (num_items * (page - 1)))
    else:
        query = 'SELECT * FROM person WHERE Name like "{0}" ORDER BY Name LIMIT {1} OFFSET {2}'\
            .format("%" + person + "%", num_items, (num_items * (page - 1)))

    p = session.query(Person).from_statement(text(query)).all()

    if person is None:
        pages = math.ceil(len(session.query(Person).all()) / num_items)
    else:
        pages = math.ceil(len(p) / num_items)

    return render_template('people/people.jade', title='People', people=p, person=person, pages=pages, page=page)


@people.route('/edit/<person_id>')
def edit_person(person_id):
    p = session.query(Person).get(person_id)
    img = (session.query(Image).from_statement(text('SELECT * '
                                                    'FROM image '
                                                    '   INNER JOIN images_people '
                                                    '    ON image.ImageId = images_people.ImageId '
                                                    '   INNER JOIN person ON images_people.PersonId = person.PersonId '
                                                    ' WHERE person.PersonId = {0}'
                                                    ' ORDER BY RANDOM()'.format(person_id))).first()).File_Path
    instagram_feed = get_instagram(p.InstagramUserName)
    if not instagram_feed:
        instagram_feed = []

    num_images = Stats.get_images_with_person(p.PersonId)
    percentage = Stats.get_images_with_person_percent(p.PersonId)
    ranking = Stats.get_person_ranking(p.PersonId)

    return render_template('people/editPerson.jade', title='Edit ' + p.Name,
                           person=p,
                           instagram=instagram_feed,
                           img=img,
                           images=num_images,
                           percentage=percentage,
                           ranking=ranking)


def get_instagram(username):
    if username:
        result = requests.get('https://www.instagram.com/' + username + '/media/')
        j = json.loads(result.text)

        for item in j['items']:
            url = item['images']['standard_resolution']['url']
            size = 's' + str(item['images']['standard_resolution']['width']) + 'x' + str(item['images']['standard_resolution']['height'])

            if size in item['images']['standard_resolution']['url']:
                url = re.sub(size + '/', '', item['images']['standard_resolution']['url'])

            item['images']['standard_resolution']['url'] = url

        return j['items']
    else:
        return None
