import libcloud.security
from flask import Flask
from flask_cloudy import Storage

from config import config
from scarlett.mod_api.controllers import api
from scarlett.mod_images.controllers import images
from scarlett.mod_main.controllers import main
from scarlett.mod_people.controllers import people
from scarlett.mod_stats.controllers import stats
from scarlett.mod_tags.controllers import tags

app = Flask(__name__, instance_relative_config=True)
app.config.update(config)
app.secret_key = config['SECRET_KEY']

# I like to live dangerously
libcloud.security.VERIFY_SSL_CERT = config['VERIFY_SSL_CERT']

# Do not import assets until app has been initialized
from .util import assets

# Set up flask-cloudy
storage = Storage()
storage.init_app(app)

# Append additional allowed file extensions
extra_extensions = ['avi', 'flv', 'mov', 'mp4', 'psd', 'webm', 'wmv']
storage.allowed_extensions.extend(extra_extensions)


app.jinja_env.add_extension('pyjade.ext.jinja.PyJadeExtension')
app.register_blueprint(api, url_prefix='/api')
app.register_blueprint(images, url_prefix='/images')
app.register_blueprint(main, url_prefix='/')
app.register_blueprint(people, url_prefix='/people')
app.register_blueprint(stats, url_prefix='/stats')
app.register_blueprint(tags, url_prefix='/tags')
