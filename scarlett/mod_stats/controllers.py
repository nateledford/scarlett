from flask import Blueprint, render_template

from ..data.stats import Stats

stats = Blueprint('stats', __name__)


@stats.route('/')
def index():
    p = Stats.get_stats_images_people()
    t = Stats.get_stats_images_tags()

    return render_template('stats/stats.jade', title='Statistics', people=p, tags=t)
