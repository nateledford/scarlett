import math
import os

from flask import Blueprint, request, render_template, jsonify
from sqlalchemy import text

from config import Config
from scarlett.data import session, engine
from scarlett.data.models.Images import Image
from scarlett.data.models.People import Person
from scarlett.util.json import AjaxResult

images = Blueprint('images', __name__)

config = Config()


@images.route('/', methods=['GET', 'POST'])
def index():
    req_tags = None
    req_people = None
    orderby = None
    favorites = False
    tags = []
    people = []

    if request.args.get('tags') is not None:
        req_tags = request.args.getlist('tags')

    if request.args.get('person') is not None:
        req_people = request.args.getlist('person')
        for p in req_people:
            people.append(session.query(Person).get(p))

    if request.args.get('orderBy') is not None:
        orderby = request.args.get('orderBy')

    if request.args.get('favorites') is not None:
        favorites = request.args.get('favorites')

    if request.args.get('next') is not None:
        page = int(request.args.get('next'))
    elif request.args.get('previous') is not None:
        page = int(request.args.get('previous'))
    elif request.args.get('first') is not None:
        page = int(request.args.get('first'))
    elif request.args.get('last') is not None:
        page = int(request.args.get('last'))
    else:
        page = int(request.args.get('currentPage'))

    query = build_query(req_tags, req_people, orderby, favorites, int(page))

    p_query = query[:-(len(query) - query.index('LIMIT'))]

    pages = math.ceil(
        len(session.query(Image).from_statement(text(p_query)).all()) / 50)

    if page > pages:
        page = pages
        query = build_query(req_tags, req_people, orderby, favorites, int(page))

    new_imgs = session.query(Image).from_statement(text(query)).all()

    ajax = {
        'template': render_template('main/includes/viewImages.jade', title='Search For Images',
                                    images=new_imgs, tags=tags,
                                    people=people,
                                    orderby=orderby,
                                    favorites=favorites,
                                    page=page,
                                    pages=pages),
        'page': page,
        'pages': pages
    }

    return jsonify(ajax)


@images.route('/<image_id>', methods=['GET', 'DELETE'])
def image(image_id):
    if request.method == 'GET':
        img = session.query(Image).get(image_id)
        img.Tags.sort(key=lambda x: x.TagName)
        return render_template('images/editImage.jade', title='Edit Image', img=img)

    if request.method == 'DELETE':
        img = session.query(Image).get(image_id)
        img_path = os.path.join(config.STORAGE_CONTAINER, img.Image_Name)
        # Delete file
        if os.path.exists(img_path):
            os.remove(img_path)

        # Delete record from database
        session.query(Image).filter(Image.ImageId == image_id).delete()
        session.commit()

        engine.execute('DELETE FROM images_tags WHERE ImageId = {0}'.format(image_id))
        engine.execute('DELETE FROM images_people WHERE ImageId = {0}'.format(image_id))
        # Delete from images albums
        # Delete from images collections

        return jsonify(AjaxResult(True, 'Image was deleted successfully').to_json())


def build_query(tags, people, orderby, favorites, page, limit=50):
    query = 'SELECT * FROM image \n'

    if people is not None:
        query += ' \tINNER JOIN images_people ON image.ImageId = images_people.ImageId\n' \
                 ' \tINNER JOIN person ON images_people.PersonId = person.PersonId\n'
    if tags is not None:
        query += ' \tINNER JOIN images_tags ON image.ImageId = images_tags.ImageId\n' \
                 ' \tINNER JOIN tag ON images_tags.TagId = tag.TagId\n'

    if tags is not None or people is not None or favorites == 'True':
        query += ' WHERE '

    if people is not None:
        query += ' image.ImageId = images_people.ImageId AND person.PersonId IN (' + ','.join(map(str, people)) + ')\n'

    if people is not None and tags is not None:
        query += ' \tAND '

    if tags is not None:
        query += ' image.ImageId = images_tags.ImageId AND (tag.TagId IN(' + ",".join(map(str, tags)) + '))\n'

    if favorites == 'True':
        if people is not None or tags is not None:
            query += ' AND '
        query += ' image.Favorite = 1\n'

    if tags is not None or people is not None:
        query += ' GROUP BY image.ImageId\n' \
                 ' HAVING COUNT(image.ImageId) = '

        count = 0
        t_count = 0
        p_count = 0
        if tags is not None:
            t_count = len(tags)

        if people is not None:
            p_count = len(people)

        if tags is not None or people is not None:
            if t_count == p_count:
                count += t_count
            elif t_count > p_count:
                count += t_count
            else:
                count += p_count

        query += str(count) + '\n'

    query += ' ORDER BY image.Uploaded'

    if orderby == 'desc':
        query += ' DESC'

    query += ' LIMIT {0} OFFSET {1}'.format(limit, (limit * (page - 1)))

    print(query)

    return query
