from flask.ext.assets import Bundle, Environment

from .. import app

assets = Environment(app)

bundles = {
    'vendor_css': Bundle(
        'bower_components/bootswatch/slate/bootstrap.css',
        'bower_components/font-awesome/css/font-awesome.css',
        'bower_components/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',
        'bower_components/select2/dist/css/select2.css',
        output='dist/vendor.css'
    ),
    'app_css': Bundle(
        'src/styles/modals/editImage.scss',
        'src/styles/thumbnails.scss',
        'src/styles/select2-theme.scss',
        'src/styles/main.scss',
        filters='pyscss, cssmin',
        output='dist/app.css'
    ),
    'vendor_js': Bundle(
        'bower_components/jquery/dist/jquery.js',
        'bower_components/bootstrap/dist/js/bootstrap.js',
        'bower_components/select2/dist/js/select2.full.js',
        'bower_components/notifyjs/dist/notify.js',
        output='dist/vendor.js'
    ),
    'app_js': Bundle(
        'src/js/main/localStorage.coffee',
        'src/js/images/editImage.coffee',
        'src/js/main/ajax.coffee',
        'src/js/main/search.coffee',
        'src/js/main/modals/settings.coffee',
        'src/js/images/open.coffee',
        'src/js/images/models/image.coffee',
        'src/js/images/modals/uploadImage.coffee',
        'src/js/images/modals/editMultipleImages.coffee',
        'src/js/people/models/person.coffee',
        'src/js/people/modals/addPerson.coffee',
        'src/js/people/modals/addPeople.coffee',
        'src/js/people/editPerson.coffee',
        'src/js/tags/models/tag.coffee',
        'src/js/tags/modals/addTags.coffee',
        filters='coffeescript',
        output='dist/app.js'
    )
}

assets.register(bundles)
