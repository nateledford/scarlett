from flask import jsonify


class JsonSerialize:
    @staticmethod
    def convert_list_to_json(obj):
        result = []
        for o in obj:
            result.append(o.to_json())
        return result


class AjaxResult:
    def __init__(self, result=None, message=None, data=None):
        if result is None:
            result = False

        if message is None:
            message = ''

        if data is None:
            data = []

        self.result = result
        self.message = message
        self.data = data

    def to_json(self):
        json = {
            'result': self.result,
            'message': self.message,
            'data': self.data
        }
        return json
