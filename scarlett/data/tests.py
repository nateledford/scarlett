import os
import unittest

import flask_cloudy

from config import config
from scarlett import app
from scarlett.data import session, Person
from scarlett.data.models.Images import Image
from scarlett.data.stats import Stats


class DataTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.session = session
        cls.storage = flask_cloudy.Storage()

        test_path = os.path.join('test', config['DB_PATH'])
        if os.path.exists(test_path):
            os.remove(test_path)

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True

    def tearDown(self):
        pass

    def test_dhash(self):
        test_image = r'./scarlett/static/img/PSD.png'
        expected_hash = 'f8e68eb2b2f8e0b4'
        dhash = Image.dhash(test_image)
        self.assertIsNotNone(dhash)
        self.assertEqual(expected_hash, dhash)

    def test_create_image(self):
        file = r'./scarlett/static/img/PSD.png'
        result = Image.create_image(self.session, self.storage, file)

        self.assertIsNotNone(result)

    def test_get_images(self):
        limit = 50
        result = Image.get_images(self.session, limit)
        self.assertIsNotNone(result)
        self.assertLessEqual(len(result), limit)

    def test_get_image_count(self):
        result = Image.get_image_count(self.session)
        self.assertIsNotNone(result)
        self.assertGreater(result, 0)

    def test_create_person(self):
        name = "Nate Ledford"
        twitter = "nateledford"

        result = Person.create_person(self.session, name, twitter=twitter)

        self.assertIsNotNone(result)
        self.assertEqual(result.Name, name)
        self.assertEqual(result.TwitterUserName, twitter)

    def test_get_people(self):
        result = Person.get_people(self.session)
        self.assertIsNotNone(result)
        self.assertGreater(len(result), 0)


class StatsTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True

    def tearDown(self):
        pass

    def test_get_total_images(self):
        result = Stats.get_total_images()
        self.assertIsNotNone(result)
        self.assertGreater(result, 0)

    def test_get_total_people(self):
        result = Stats.get_total_people()
        self.assertIsNotNone(result)
        self.assertGreater(result, 0)

    def test_get_images_with_people(self):
        result = Stats.get_images_with_people()
        self.assertIsNotNone(result)
        self.assertGreater(result, 0)

    def test_get_images_with_person(self):
        person = 1
        result = Stats.get_images_with_person(person)
        self.assertIsNotNone(result)
        self.assertGreater(result, 0)

    def test_get_images_with_person_percent(self):
        person = 1
        result = Stats.get_images_with_person_percent(person)
        self.assertIsNotNone(result)
        self.assertGreater(result, 0)

    def test_get_stats_images_people(self):
        result = Stats.get_stats_images_people()
        self.assertIsNotNone(result)

    def test_get_people_ranking(self):
        person = 1
        result = Stats.get_person_ranking(person)
        self.assertIsNotNone(result)

    def test_get_images_with_tags(self):
        result = Stats.get_images_with_tags()
        self.assertIsNotNone(result)
        self.assertGreater(result, 0)

    def test_get_images_with_tag(self):
        tag = 1
        result = Stats.get_images_with_tag(tag)
        self.assertIsNotNone(result)
        self.assertGreater(result, 0)

    def test_get_stats_images_tags(self):
        result = Stats.get_stats_images_tags()
        self.assertIsNotNone(result)
