import statistics
from decimal import Decimal

from scarlett.data import session, engine, Tag
from scarlett.data.models.Images import Image
from scarlett.data.models.People import Person

PLACES = Decimal(10) ** -4


class Stats:
    @staticmethod
    def get_total_images():
        count = session.query(Image).count()
        return count

    @staticmethod
    def get_total_tags():
        count = session.query(Tag).count()
        return count

    @staticmethod
    def get_total_people():
        count = session.query(Person).count()
        return count

    @staticmethod
    def get_images_with_people():
        count = session.query(Image).filter(Image.People.any()).count()
        return count

    @staticmethod
    def get_images_with_person(person_id):
        count = session.query(Image).filter(Image.People.any(Person.PersonId == person_id)).count()

        return count

    @staticmethod
    def get_stats_images_people():
        query = 'SELECT images_people.PersonId, COUNT(image.ImageId) AS [NumImages]' \
                ' FROM image' \
                ' INNER JOIN images_people ON image.ImageId = images_people.ImageId' \
                ' INNER JOIN person ON images_people.PersonId = person.PersonId' \
                ' GROUP BY images_people.PersonId' \
                ' ORDER BY NumImages DESC, person.Name'

        result = engine.execute(query)

        counts = []
        people = []
        max = 0
        for row in result:
            if max < 25:
                p = PeopleStats()
                rp = session.query(Person).get(int(row[0]))
                p.PersonId = rp.PersonId
                p.Name = rp.Name
                p.Images = (int(row[1]))
                p.Percentage = Stats.get_images_with_person_percent(int(row[0]))
                people.append(p)

            counts.append(int(row[1]))
            max += 1

        stats = ImageStats()

        if max > 0:
            stats.Average = statistics.mean(counts)
            stats.Median = statistics.median(counts)

            try:
                stats.Mode = statistics.mode(counts)
            except statistics.StatisticsError:
                stats.Mode = 'N/A'

            stats.StdDev = statistics.pstdev(counts)
            stats.Variance = statistics.pvariance(counts)
            stats.Lower = stats.Average - (2 * stats.StdDev)
            stats.Higher = stats.Average + (2 * stats.StdDev)

        stats.TopPeople = people
        return stats

    @staticmethod
    def get_images_with_person_percent(person_id):
        query = 'SELECT images_people.PersonId, COUNT(image.ImageId) AS [NumImages],' \
                ' (COUNT(image.ImageId)*1.0/ (' \
                '    SELECT COUNT(*) FROM image INNER JOIN images_people ON image.ImageId = images_people.ImageId)' \
                ' ) * 100 AS Percentage' \
                ' FROM image' \
                ' INNER JOIN images_people ON image.ImageId = images_people.ImageId ' \
                ' WHERE images_people.PersonId = ' + str(person_id) + ' ' \
                ' GROUP BY images_people.PersonId ' \
                ' ORDER BY NumImages DESC, images_people.PersonId'

        result = engine.execute(query)

        percentage = 0
        for row in result:
            percentage = Decimal(row[2]).quantize(PLACES)

        return percentage

    @staticmethod
    def get_person_ranking(person_id):
        rankings = engine.execute('SELECT person.PersonId, COUNT(image.ImageId) AS [NumImages] '
                                  'FROM image '
                                  '  INNER JOIN images_people ON image.ImageId = images_people.ImageId '
                                  '  INNER JOIN person ON images_people.PersonId = person.PersonId '
                                  'GROUP BY person.PersonId '
                                  'ORDER BY NumImages DESC')

        ranking = Ranking(1, Stats.get_total_people())
        for row in rankings:
            if row[0] == person_id:
                return ranking
            ranking.Rank += 1

        return ranking

    @staticmethod
    def get_images_with_tags():
        count = session.query(Image).filter(Image.Tags.any()).count()
        return count

    @staticmethod
    def get_images_with_tag(tag_id):
        count = session.query(Image).filter(Image.Tags.any(Tag.TagId == tag_id)).count()
        return count

    @staticmethod
    def get_stats_images_tags():
        query = 'SELECT tag.TagId, tag.TagName, COUNT(image.ImageId) AS [NumImages],' \
                ' (COUNT(tag.TagId) * 1.0/' \
                '  (SELECT COUNT(*) FROM tag INNER JOIN images_tags ON tag.TagId = images_tags.TagId)' \
                '  ) * 100 AS Percentage' \
                ' FROM tag' \
                ' INNER JOIN images_tags ON tag.TagId = images_tags.TagId' \
                ' INNER JOIN image ON images_tags.ImageId = image.ImageId' \
                ' GROUP BY tag.TagId' \
                ' ORDER BY NumImages DESC, tag.TagName'

        result = engine.execute(query)

        counts = []
        tags = []
        max = 0
        for row in result:
            if max < 25:
                t = TagStat()
                t.TagId = row[0]
                t.TagName = row[1]
                t.Images = row[2]
                t.Percentage = row[3]
                tags.append(t)

            counts.append(int(row[2]))
            max += 1

        stats = TagStats()

        if max > 0:
            stats.Average = statistics.mean(counts)
            stats.Median = statistics.median(counts)

            try:
                stats.Mode = statistics.mode(counts)
            except statistics.StatisticsError:
                stats.Mode = 'N/A'

            stats.StdDev = statistics.pstdev(counts)
            stats.Variance = statistics.pvariance(counts)
            stats.Lower = stats.Average - (2 * stats.StdDev)
            stats.Higher = stats.Average + (2 * stats.StdDev)

        stats.TopTags = tags
        return stats


class Ranking:
    def __init__(self, ranking, total_people):
        self.Rank = ranking
        self.TotalPeople = total_people


class AllStats:
    def __init__(self, avg=None, median=None, mode=None, stddev=None, var=None, lower=None, higher=None):
        self.Average = avg
        self.Median = median
        self.Mode = mode
        self.StdDev = stddev
        self.Variance = var
        self.Lower = lower
        self.Higher = higher


class ImageStats(AllStats):
    def __init__(self, avg=None, median=None, mode=None, stddev=None, var=None, lower=None, higher=None,
                 top_people=None):
        super().__init__(avg, median, mode, stddev, var, lower, higher)
        if top_people is None:
            top_people = []

        self.People = session.query(Person).filter(Person.Images.any()).count()
        self.Images = Stats.get_images_with_people()
        self.TopPeople = top_people


class TagStats(AllStats):
    def __init__(self, avg=None, median=None, mode=None, stddev=None, var=None, lower=None, higher=None, top_tags=None):
        super().__init__(avg, median, mode, stddev, var, lower, higher)

        self.Tags = session.query(Tag).filter(Tag.Images.any()).count()
        self.Images = Stats.get_images_with_tags()
        self.TopTags = top_tags


class PeopleStats:
    def __init__(self, person_id=None, name=None, images=None, percentage=None):
        self.PersonId = person_id
        self.Name = name
        self.Images = images
        self.Percentage = percentage


class TagStat:
    def __init__(self, tag_id=None, name=None, images=None, percentage=None):
        self.TagId = tag_id
        self.TagName = name
        self.Images = images
        self.Percentage = percentage
