from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from config import config
from scarlett.data.models.Albums import Album
from scarlett.data.models.Collections import Collection
from scarlett.data.models.Images import Image
from scarlett.data.models.People import Person
from scarlett.data.models.Tags import Tag
from scarlett.data.models.tables import images_people, images_tags, images_collections
from .base import Base

engine = create_engine('sqlite:///' + config['DB_PATH'])

Base.metadata.create_all(engine)

db_session = sessionmaker(bind=engine)
session = db_session()
