from sqlalchemy import Column, Integer, String, Boolean
from sqlalchemy.orm import relationship

from scarlett.data.base import Base
from scarlett.data.models.tables import images_people


class Person(Base):
    __tablename__ = 'person'
    PersonId = Column(Integer, primary_key=True)
    Name = Column(String(100), nullable=False)
    Favorite = Column(Boolean)
    InstagramUserName = Column(String(100), nullable=True)
    FacebookUserName = Column(String(100), nullable=True)
    TwitterUserName = Column(String(100), nullable=True)

    Images = relationship('Image',
                          secondary=images_people,
                          backref='people')

    def to_json(self):
        json = {
            "PersonId": self.PersonId,
            "Name": self.Name,
            "Favorite": self.Favorite
        }
        return json

    @staticmethod
    def get_people(session):
        people = session.query(Person).order_by(Person.Name).all()
        return people

    @staticmethod
    def create_person(session, name, favorite=False, facebook=None, instagram=None, twitter=None):
        p = Person()
        p.Name = name
        p.Favorite = favorite
        p.FacebookUserName = facebook
        p.InstagramUserName = instagram
        p.TwitterUserName = twitter

        session.add(p)
        session.commit()
        return p
