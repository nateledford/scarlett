from sqlalchemy import Column, Integer, String, Boolean

from scarlett.data.base import Base


class Album(Base):
    __tablename__ = 'album'
    AlbumId = Column(Integer, primary_key=True)
    AlbumName = Column(String(100), nullable=False)
    Favorite = Column(Boolean)

    def to_json(self):
        json = {
            'AlbumId': self.AlbumId,
            'AlbumName': self.AlbumName,
            'Favorite': self.Favorite
        }
        return json
