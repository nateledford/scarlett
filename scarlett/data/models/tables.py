from sqlalchemy import Table, Column, Integer, ForeignKey
from scarlett.data.base import Base

images_tags = Table('images_tags', Base.metadata,
                    Column('Id', Integer, primary_key=True),
                    Column('ImageId', Integer, ForeignKey('image.ImageId')),
                    Column('TagId', Integer, ForeignKey('tag.TagId'))
                    )

images_people = Table('images_people', Base.metadata,
                      Column('Id', Integer, primary_key=True),
                      Column('ImageId', Integer, ForeignKey('image.ImageId')),
                      Column('PersonId', Integer, ForeignKey('person.PersonId'))
                      )

images_collections = Table('images_collections', Base.metadata,
                           Column('Id', Integer, primary_key=True),
                           Column('ImageId', Integer, ForeignKey('image.ImageId')),
                           Column('CollectionId', Integer, ForeignKey('collection.CollectionId'))
                           )
