import datetime
import os
import shutil
import subprocess
import tempfile

import requests
# noinspection PyPackageRequirements
from PIL import Image as pImage
from flask import jsonify
from flask.ext.sqlalchemy import xrange
from sqlalchemy import Column, ForeignKey, Integer, String, DateTime, Boolean
from sqlalchemy.orm import relationship

from config import config
from scarlett.data.models.People import Person
from scarlett.data.models.Tags import Tag
from scarlett.data.base import Base
from scarlett.data.models.tables import images_tags, images_people, images_collections
from scarlett.util.json import JsonSerialize, AjaxResult


class Image(Base):
    __tablename__ = 'image'
    ImageId = Column(Integer, primary_key=True)
    Image_Name = Column(String(250), nullable=False)
    File_Path = Column(String(250), nullable=False)
    Hash = Column(String(250), nullable=False)
    Uploaded = Column(DateTime, nullable=False)
    Favorite = Column(Boolean)

    Tags = relationship('Tag',
                        secondary=images_tags,
                        backref='images')

    People = relationship('Person',
                          secondary=images_people,
                          backref='images')

    Collection = relationship('Collection',
                              secondary=images_collections,
                              backref='collections')

    AlbumId = Column(Integer, ForeignKey('album.AlbumId'))
    Album = relationship('Album', backref='image')

    def to_json(self):
        json = {
            "ImageId": self.ImageId,
            "Image_Name": self.Image_Name,
            "File_Path": self.File_Path,
            "Uploaded": self.Uploaded,
            "Favorite": self.Favorite,
            "Tags": JsonSerialize.convert_list_to_json(self.Tags),
            "People": JsonSerialize.convert_list_to_json(self.People)
        }
        return json

    @staticmethod
    def dhash(image, hash_size=8):
        """
        SOURCE: http://blog.iconfinder.com/detecting-duplicate-images-using-python/

        :param hash_size:
        :param image:
        """
        image = pImage.open(image)

        # Grayscale and shrink the image in one step
        image = image.convert('L').resize(
            (hash_size + 1, hash_size),
            pImage.ANTIALIAS
        )

        # Compare adjacent pixels
        difference = []
        for row in xrange(hash_size):
            for col in xrange(hash_size):
                pixel_left = image.getpixel((col, row))
                pixel_right = image.getpixel((col + 1, row))
                difference.append(pixel_left > pixel_right)

        # Convert the binary array to a hexadecimal string
        decimal_value = 0
        hex_string = []
        for index, value in enumerate(difference):
            if value:
                decimal_value += 2**(index % 8)
            if (index % 8) == 7:
                hex_string.append(hex(decimal_value)[2:].rjust(2, '0'))
                decimal_value = 0

        return ''.join(hex_string)

    @staticmethod
    def save_remote_file_to_temp(url):
        """Saves an image to the user's hard disk

        :param url:    The URL of the remote image to save
        """
        temp = tempfile.mkdtemp()
        file_name = os.path.basename(url)
        response = requests.get(url, stream=True)
        new_path = os.path.join(temp, file_name)
        with open(new_path, 'wb') as out_file:
            shutil.copyfileobj(response.raw, out_file)
        return new_path

    @staticmethod
    def open_images(session, image_ids):
        file_names = []
        for i in image_ids:
            file_names.append((session.query(Image).get(i)).Image_Name)

        new_path = Image.__copy_images_to_temp(file_names)
        subprocess.call("open " + new_path, shell=True)

    @staticmethod
    def __copy_images_to_temp(files):
        """Copies a collection of images to a temporary folder on the user's hard disk

        :param files:   The collection of images to copy
        """

        # Assign temp path to variable
        temp = os.path.join(tempfile.gettempdir(), 'scarlett')

        # Delete the current temporary directory if it already exists
        if os.path.exists(temp):
            shutil.rmtree(temp)

        # Create the temporary directory
        os.makedirs(temp)

        # Store the temporary paths of the images
        new_paths = []

        # If there is a collection of multiple images, copy them one at a time
        if isinstance(files, list):
            for f in files:
                new_paths.append(Image.__copy_image_to_temp(f, temp))
        else:
            new_paths.append(Image.__copy_image_to_temp(files, temp))

        # Return the new path of the first image in the collection
        return new_paths[0]

    @staticmethod
    def __copy_image_to_temp(file, temp):
        """Copies a single image to the temporary directory

        :param file:    The image to copy
        """
        file = os.path.join(config['STORAGE_CONTAINER'], file)
        file_name = os.path.basename(file)
        new_path = os.path.join(temp, file_name)
        shutil.copyfile(file, new_path)
        return new_path

    @staticmethod
    def __build_image(session, img_upload, ts=None, ps=None):
        """Builds an Image object to be stored in the database

        :param session      A current SqlAlchemy session
        :param img_upload   Metadata about the image already uploaded to storage
        :param ts           A list of tags to be attached to the image
        :param ps           A list of poeple to be attached to the image
        """
        if ts is None:
            ts = []

        if ps is None:
            ps = []

        i = Image()
        i.Image_Name = img_upload.name
        i.File_Path = img_upload.url

        if img_upload.extension not in ['mov', 'psd', 'mp4']:
            img_path = os.path.join(config['STORAGE_CONTAINER'], i.Image_Name)
            # noinspection PyTypeChecker
            i.Hash = Image.dhash(img_path)
        else:
            i.Hash = 'N/A'
        i.Uploaded = datetime.datetime.today().now()
        i.Favorite = 0

        # Check if the image has already been uploaded to database
        if session.query(Image).filter(Image.Hash == i.Hash).count() > 0:
            os.remove(os.path.join(config['STORAGE_CONTAINER'], i.Image_Name))
            return jsonify(AjaxResult(False, 'Image already exists in database').to_json())

        for t in ts:
            new_tag = session.query(Tag).get(t)
            i.Tags.append(new_tag)

        for p in ps:
            new_person = session.query(Person).get(p)
            i.People.append(new_person)

        session.add(i)
        session.commit()

        return i.to_json()

    @staticmethod
    def get_images(session, limit=50):
        imgs = session.query(Image).order_by(Image.Uploaded.desc()).limit(limit).all()
        return imgs

    @staticmethod
    def get_image(session, img_id):
        img = session.query(Image).get(img_id)
        return img

    @staticmethod
    def get_image_count(session):
        count = session.query(Image).count()
        return count

    @staticmethod
    def create_image(session, storage, file, ts=None, ps=None):
        upload = storage.upload(file)
        img = Image.__build_image(session, upload, ts, ps)
        return img

    @staticmethod
    def create_images(storage, session, local_dir, ts=None, ps=None):
        count = 0
        if os.path.exists(local_dir):
            for root, dirs, files in os.walk(local_dir):
                for name in files:
                    file_name, file_extension = os.path.splitext(name)
                    if not name.startswith('._') and file_extension not in ['.db', '.ini']:
                        img_path = os.path.join(root, name)
                        img_upload = storage.upload(img_path)
                        Image.__build_image(session, img_upload, ts, ps)
                        count += 1
        return count

    @staticmethod
    def create_image_from_url(storage, session, url, ts=None, ps=None):
        upload = storage.upload(Image.save_remote_file_to_temp(url))
        img = Image.__build_image(session, upload, ts, ps)
        return img

    @staticmethod
    def add_tags(session, img_ids, tag_ids):
        """Adds tags to one or more images

        :param session      A current SqlAlchemy session
        :param img_ids      A list of images IDs in the database
        :param tag_ids      A list of tag IDs in the database
        """
        for tag_id in tag_ids:
            t = session.query(Tag).get(tag_id)

            for img_id in img_ids:
                img = session.query(Image).get(img_id)

                if t not in img.Tags:
                    img.Tags.append(t)
                    session.commit()
        return True

    @staticmethod
    def remove_tag(session, img_id, tag_id):
        """Remove tag from an images

        :param session      A current SqlAlchemy session
        :param img_id       An image in the database
        :param tag_id       A tag in the database
        """
        img = session.query(Image).get(img_id)
        t = session.query(Tag).get(tag_id)

        img.Tags.remove(t)
        session.commit()
        return AjaxResult(True, 'Tag removed from image successfully', img.to_json()).to_json()

    @staticmethod
    def add_person(session, img_id, person_id):
        """Attaches a person to an image

        :param session      A current SqlAlchemy session
        :param img_id       An image in the database
        :param person_id    A person in the database
        :returns            An AjaxResult object
        """
        img = session.query(Image).get(img_id)
        p = session.query(Person).get(person_id)

        img.People.append(p)
        session.commit()
        return img

    @staticmethod
    def remove_person(session, img_id, person_id):
        """Removes a person to an image

        :param session      A current SqlAlchemy session
        :param img_id       An image in the database
        :param person_id    A person in the database
        :returns            An AjaxResult object
        """
        img = session.query(Image).get(img_id)
        p = session.query(Person).get(person_id)

        img.People.remove(p)
        session.commit()
        return img

