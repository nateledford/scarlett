from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from scarlett.data.base import Base
from scarlett.data.models.tables import images_tags
from scarlett.util.json import AjaxResult


class Tag(Base):
    __tablename__ = 'tag'
    TagId = Column(Integer, primary_key=True)
    TagName = Column(String(250), nullable=False)

    Images = relationship('Image',
                          secondary=images_tags,
                          backref='tags')

    def to_json(self):
        json = {
            "TagId": self.TagId,
            "TagName": self.TagName
        }
        return json

    @staticmethod
    def get_tags(session):
        ts = session.query(Tag).order_by(Tag.TagName).all()
        return ts

    @staticmethod
    def search_tags(session, query):
        ts = session.query(Tag).filter(Tag.TagName.like('%' + query + '%')).all()
        return ts

    @staticmethod
    def get_tag(session, tag_id):
        t = session.query(Tag).get(tag_id)
        return t

    @staticmethod
    def create_tag(session, tag_name):
        t = Tag()
        t.TagName = tag_name

        if session.query(Tag).filter(Tag.TagName.like(t.TagName)).count() > 0:
            return False

        session.add(t)
        session.commit()
        return True

    @staticmethod
    def delete_tag(session, tag_id):
        t = session.query(Tag).get(tag_id)
        session.remove(t)
        session.commit()
        return True

    @staticmethod
    def update_tag(session, tag_id, tag_name):
        t = session.query(Tag).get(tag_id)
        t.TagName = tag_name
        session.commit()
        return True
