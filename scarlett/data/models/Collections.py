from sqlalchemy import Column, Integer, String, Boolean
from sqlalchemy.orm import relationship

from scarlett.data.base import Base
from scarlett.data.models.tables import images_collections


class Collection(Base):
    __tablename__ = 'collection'
    CollectionId = Column(Integer, primary_key=True)
    CollectionName = Column(String, nullable=False)
    Favorite = Column(Boolean)

    Images = relationship('Image',
                          secondary=images_collections,
                          backref='collections')
