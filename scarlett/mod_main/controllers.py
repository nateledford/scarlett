import math

from flask import Blueprint, render_template, request

from scarlett.data import session
from scarlett.data.models.Images import Image

main = Blueprint('main', __name__)


@main.route('/', methods=['GET'])
def index():
    if request.method == 'GET':
        imgs = Image.get_images(session)

        pages = math.ceil(Image.get_image_count(session) / 50)

        return render_template('main/index.jade',
                               title='Home',
                               images=imgs,
                               page=1,
                               pages=pages,
                               orderby='desc')
