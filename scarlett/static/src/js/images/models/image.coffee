class Image
  constructor: (@imageId, @name, @path, @timestamp, @tags, @person) ->

  @deleteImage = (imageId, col) ->
    $.ajax
      url: '/images/' + imageId
      method: 'DELETE'
      success: (data) ->
        $.notify(data.message)
        if col
          col.remove()

  @addPerson = (imgId, personId) ->
    $.ajax
      url: '/api/images/people?imageId=' + imgId + '&personId=' + personId + '&action=add'
      method: 'POST'
      success: (data) ->
        $.notify data.message

  @removePerson = (imgId, personId) ->
    $.ajax
      url: '/api/images/people?imageId=' + imgId + '&personId=' + personId + '&action=remove'
      method: 'POST'
      success: (data) ->
        $.notify data.message