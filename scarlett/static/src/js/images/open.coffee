$ ->
  $('.open-img-button').each ->
    $(this).on 'click', ->
      $id = $(this).closest('.thumbnail').data('imageid')
      $.ajax
        url: '/api/image/open/' + $id
        method: 'POST'
        success: (data) ->
          $.notify(data.result)
