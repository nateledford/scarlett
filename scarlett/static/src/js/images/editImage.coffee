class EditImage
  origTags = []
  origPeople = []
  selectTags = $('#currentTags')
  selectPeople = $('#currentPerson')
  chkFavorite = $('#chkFavorite')

  @activateCurrentTags = ->
    selectTags.select2
      placeholder: "Search by tags..."
      ajax:
        url: "/api/tags"
        dataType: "json"
        delay: 250
        data: (params) ->
          q: params.term
          page: params.page

        processResults: (data) ->
          results: $.map(data.data, (tag) ->
            text: tag.TagName
            id: tag.TagId
          )

      minimumInputLength: 1

    EditImage.origTags = selectTags.val()

    selectTags.on 'change', () ->
      EditImage.updateTags($(this).val(), EditImage.origTags, $(this).data('imageid'))

    return


  @activateCurrentPerson = ->
    $("#currentPerson").select2
      placeholder: "Search by person..."
      allowClear: true
      ajax:
        url: "/api/people"
        dataType: "json"
        delay: 250
        data: (params) ->
          q: params.term
          page: params.page

        processResults: (data) ->
          results: $.map(data.people, (person) ->
            text: person.Name
            id: person.PersonId
          )

      minimumInputLength: 1

    EditImage.origPeople = selectPeople.val()

    selectPeople.on 'change', () ->
      EditImage.updatePeople($(this).val(), EditImage.origPeople, $(this).data('imageid'))

  @updateTags = (newTags, origTags, imgId) ->
    if newTags is null
      console.log 'TODO user must select one tag'
      return

    if origTags is null
      console.log 'TODO add new tag'
      return

    if newTags.length > origTags.length
      difference = $.grep newTags, (el) ->
        $.inArray(el, origTags) == -1

      $.ajax
        url: '/api/image/tags?imageId=' + imgId + '&tagId=' + difference + '&action=add'
        method: 'POST'
        success: (data) ->
          $.notify data.message

    else
      difference = $.grep origTags, (el) ->
        $.inArray(el, newTags) == -1

      $.ajax
        url: '/api/image/tags?imageId=' + imgId + '&tagId=' + difference + '&action=remove'
        method: 'POST'
        success: (data) ->
          $.notify data.message

    EditImage.origTags = $('#currentTags').val()

  @updatePeople = (newPeople, origPeople, imgId) ->
    console.log 'New People'
    console.log newPeople

    console.log 'Original People'
    console.log origPeople

    if newPeople is null
      Image.removePerson(imgId, origPeople[0])
      return

    if origPeople is null or newPeople.length == origPeople.length
      Image.addPerson(imgId, newPeople[0])
      return

    if newPeople.length > origPeople.length
      difference = $.grep newPeople, (el) ->
        $.inArray(el, origPeople) == -1
      Image.addPerson(imgId, difference)

    else if newPeople.length < origPeople.length
      difference = $.grep origPeople, (el) ->
        $.inArray(el, newPeople) == -1

      console.log(difference)
      Image.removePerson(imgId, difference)

    EditImage.origPeople = []

  @updateFavorite = (imageId) ->
    $checked = chkFavorite.is(':checked')
    console.log($checked)

    $.ajax
      url: '/api/image/favorite'
      method: 'POST'
      data:
        imgId: imageId
        favorite: !$checked
      success: (data) ->
        console.log(data)

$('#chkFavorite').on 'change', () ->
  EditImage.updateFavorite($(this).data('imageid'))