class UploadImage
  @modal = $('#uploadImageModal')
  @form = this.modal.find('form')

  @tabs = this.modal.find('.nav-tabs')

  @localImg = this.form.find('input[name="img"]')
  @localDir = this.form.find('input[name="dir"]')
  @remoteImg = this.form.find('input[name="url"]')

  @selectPeople = this.modal.find('select[name="people"]')
  @selectTags = this.modal.find('select[name="tags"]')

  @upload = this.modal.find('.btn-primary')
  @close = this.modal.find('.btn-default')

  @setUpFormEvents = ->
    UploadImage.modal.on "show.bs.modal", (e) ->
      $btn = $(e.relatedTarget)
      AppAjax.buildTagsSelectMenu(UploadImage.selectTags)
      AppAjax.buildPersonSelectMenu(UploadImage.selectPeople)

      # From Instagram Feed
      if $btn.hasClass('download-insta')
        person = $btn.data('personid')
        name = $btn.data('name')
        url = $btn.data('img')
        UploadImage.tabs.find('a[href="#remote"]').tab('show')

        UploadImage.remoteImg.val(url)
        UploadImage.selectPeople.append($('<option selected/>').val(person).text(name)).trigger('change')

    UploadImage.upload.on "click", ->
      UploadImage.form.submit()

    UploadImage.form.on "submit", (e) ->
      form = $(this)
      formdata = false
      formdata = new FormData(form[0])  if window.FormData
      url = null
      if UploadImage.remoteImg.val() != ''
        url = '/api/image/remote'
      else if UploadImage.localDir.val() != ''
        url = '/api/images/local'
      else
        url = '/api/image/local'

      $(this).addClass('hidden')
      $('#uploading').removeClass('hidden')
      UploadImage.close.attr('disabled','disabled')
      UploadImage.upload.attr('disabled', 'disabled')

      $.ajax
        type: "POST"
        url: url
        cache: false
        data: (if formdata then formdata else form.serialize())
        contentType: false
        processData: false
        success: (data) ->
          $('#uploadImage').find('input[type="file"]').val('')
          $.notify(data.message, if data.result is true then "success" else "warn")
          $("#uploadImageModal").modal "hide"
          $('#searchImages').submit()

      e.preventDefault()

    UploadImage.modal.on 'hide.bs.modal', ->
      UploadImage.form.removeClass('hidden')
      $('#uploading').addClass('hidden')
      UploadImage.close.removeAttr('disabled')
      UploadImage.upload.removeAttr('disabled')

      UploadImage.localDir.val('')
      UploadImage.selectPeople.val('')
      UploadImage.selectTags.val('')

$ ->
  UploadImage.setUpFormEvents()
