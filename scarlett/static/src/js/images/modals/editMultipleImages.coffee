$ ->
  $modal = $('#editMultipleImagesModal')
  $thumbnailsList = $modal.find('ul.list-inline')
  $tags = $modal.find('select[name="tags"]')

  # TODO attach entity to multiple images
  $person = $modal.find('select[name="person"]')

  $modal.on 'show.bs.modal', (event) ->
    images = AppLocalStorage.getImagesToEdit()

    # Load images
    i = 0
    while i < images.length
      $.ajax
        url: '/api/image/local'
        type: 'GET'
        data:
          imageId: images[i]
        success: (data) ->
          $li = $("<li/>").append($("<img/>").attr('src', data.image.File_Path).attr('width', '100px').attr('height', '100px'))
          $thumbnailsList.append($li)

      i++

    AppAjax.buildTagsSelectMenu($tags)
    AppAjax.buildPersonSelectMenu($person)

  $btnSaveChanges = $modal.find('button.btn-primary')
  $btnSaveChanges.on 'click', ->

    $.ajax
      url: '/api/images/tags'
      method: 'POST'
      data:
        imageIds: AppLocalStorage.getImagesToEdit()
        tagIds: $tags.val()
      success: (data) ->
        $.notify(data.message)
        $tags.val('')
        $modal.modal('hide')
