$ ->
  $modal = $("#addPersonModal")
  $btn = $("#btnAddPerson")
  $modal.on "show.bs.modal", ->
    $("span#add-person-error").addClass "hidden"
    txt = $(this).find("input[type=\"text\"]")
    txt.val ""

    $('#txtAddPerson').keyup (event) ->
      if event.keyCode is 13
        $btn.click()

  $btn.on "click", ->
    person = $modal.find("input[type=\"text\"]").val()
    $.ajax
      url: "/api/person"
      type: "POST"
      data:
        name: person

      success: (data) ->
        console.log data
        $("span#add-person-error").removeClass "hidden"
        if data.result is 0
          $.notify "Person already exists in database."
        else
          $.notify "Person added successfully!"

        $modal.find("input[type=\"text\"]").val ""