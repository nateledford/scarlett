$ ->
  $txt = $('#txtAddPeople')
  $btn = $('#btnAddPeople')

  $btn.on 'click', ->
    dir = $txt.val()
    $.ajax
      url: '/api/people'
      type: 'POST'
      data:
        directory: dir
      success: (data) ->
        if data.count > 0
          $txt.notify(data.message, "success")
        else
          $txt.notify(data.message, "warn")
        $txt.val("")