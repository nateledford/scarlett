$ ->
  $form = $('form#editPerson')

  $name = $form.find('input[name="name"]')
  $facebook = $form.find('input[name="facebook"]')
  $instagram = $form.find('input[name="instagram"]')
  $twitter = $form.find('input[name="twitter"]')

  $btnSaveChanges = $form.find('.btn-primary')

  $btnSaveChanges.on 'click', ->
    $.ajax
      url: '/api/person/' + $form.data('personid')
      method: 'PUT'
      data:
        name: $name.val()
        facebook: $facebook.val()
        instagram: $instagram.val()
        twitter: $twitter.val()
      success: (data) ->
        $.notify(data.message)
        document.location.reload(true)
