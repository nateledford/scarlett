class AppAjax
  @buildPersonSelectMenu = (element) ->
    element.select2
      placeholder: "Search by entity..."
      tags: true
      ajax:
        url: "/api/people"
        dataType: "json"
        delay: 250
        data: (params) ->
          q: params.term
          page: params.page

        processResults: (data) ->
          results: $.map(data.people, (p) ->
            text: p.Name
            id: p.PersonId
          )

      minimumInputLength: 1

  @buildTagsSelectMenu = (element) ->
    element.select2
      placeholder: "Search by tags..."
      allowClear: true
      tags: true
      ajax:
        url: "/api/tags"
        dataType: "json"
        delay: 250
        data: (params) ->
          q: params.term
          page: params.page

        processResults: (data) ->
          results: $.map(data.data, (tag) ->
            text: tag.TagName
            id: tag.TagId
          )

      minimumInputLength: 1