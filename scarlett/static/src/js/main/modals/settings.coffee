$ ->
  $modal = $('#settingsModal')
  $btnSave = $modal.find('.btn-primary')

  $form = $modal.find('form')
  $debug = $form.find('input[name="debug"]')
  $assets = $form.find('input[name="assets"]')
  $db = $form.find('input[name="db"]')
  $port = $form.find('input[name="port"]')
  $container = $form.find('input[name="container"]')

  $modal.on 'show.bs.modal', (e) ->
    $.ajax
      url: '/api/settings'
      success: (data) ->
        config = data.data

        if config.DEBUG is true
          $debug.attr('checked', 'checked')

        if config.ASSETS_DEBUG is true
          $assets.attr('checked', 'checked')

        $db.val(config.DB_PATH)
        $port.val(config.PORT)
        $container.val(config.STORAGE_CONTAINER)

  $btnSave.on 'click', ->
    $.ajax
      url: '/api/settings'
      method: 'POST'
      data: $form.serialize()
      success: (data) ->
        $.notify(data.message)




