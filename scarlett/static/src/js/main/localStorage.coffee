class AppLocalStorage
  @addCurrentImages = (images) ->
    localStorage.setItem 'currentImages', JSON.stringify(images)
  @getCurrentImages = ->
    JSON.parse localStorage.getItem('currentImages')
  @clearImages = ->
    localStorage.setItem 'currentImages', ""

  @addCurrentTags = (tags) ->
    localStorage.setItem 'currentTags', JSON.stringify(tags)
  @getCurrentTags = ->
    JSON.parse localStorage.getItem('currentTags')

  @addCurrentPeople = (people) ->
    localStorage.setItem 'currentPeople', JSON.stringify(people)
  @getCurrentPeople = ->
    JSON.parse localStorage.getItem('currentPeople')

  @setCurrentOrder = (order) ->
    localStorage.setItem 'currentOrder', JSON.stringify(order)
  @getCurrentOrder = ->
    JSON.parse localStorage.getItem('currentOrder')

  @setFavoritesOnly = (favs) ->
    localStorage.setItem 'currentFavs', JSON.stringify(favs)
  @getFavoritesOnly = ->
    JSON.parse localStorage.getItem('currentFavs')

  @getImagesToEdit = ->
    JSON.parse localStorage.getItem('imagesToEdit')
  @setImagesToEdit = (images) ->
    localStorage.setItem 'imagesToEdit', JSON.stringify(images)
  @clearImagesToEdit = ->
    localStorage.setItem 'imagesToEdit', ''
