class Search
  @form = $('#searchImages')
  @favs = Search.form.find('input[name="favorites"]')
  @people = Search.form.find('select[name="person"]')
  @tags = Search.form.find('select[name="tags"]')
  @orderBy = Search.form.find('select[name="orderBy"]')
  @openImages = Search.form.find('a.btn-primary')

  @fav = Search.form.find('input[name="favorites"]')

  @first = Search.form.find('button.first')
  @firstHidden = Search.form.find('input[name="first"]')

  @previous = Search.form.find('button.previous')
  @prevHidden = Search.form.find('input[name="previous"]')

  @next = Search.form.find('button.next')
  @nextHidden = Search.form.find('input[name="next"]')

  @last = Search.form.find('button.last')
  @lastHidden = Search.form.find('input[name="last"]')

  @currentPage = Search.form.find('input[name="currentPage"]')

  @setUpForm = ->
    Search.orderBy.select2()

    Search.initializeForm()
    Search.setUpFormEvents()
    Search.activateFavoriteButton()
    Search.deleteImage()
    Search.editMultipleImages()
    Search.viewPerson()

  @initializeForm = ->
    AppAjax.buildPersonSelectMenu(this.people)
    AppAjax.buildTagsSelectMenu(this.tags)

    people = AppLocalStorage.getCurrentPeople()
    tags = AppLocalStorage.getCurrentTags()
    orderBy = AppLocalStorage.getCurrentOrder()

    Search.orderBy.val(orderBy)

    i = 0
    if people != null and people.length > 0
      while i < people.length
        $.ajax
          url: '/api/person/' + people[i]
          type: 'GET'
          success: (data) ->
            id = data.data.PersonId
            name = data.data.Name
            Search.people.append($('<option selected/>').val(id).text(name))
            Search.people.trigger('change')
        i++

    i = 0
    if tags != null and tags.length > 0
      while i < tags.length
        $.ajax
          url: '/api/tag/' + tags[i]
          type: 'GET'
          success: (data) ->
            id = data.data.TagId
            name = data.data.TagName
            Search.tags.append($('<option selected/>').val(id).text(name))
            Search.tags.trigger('change')
        i++

  @setUpFormEvents = ->
    Search.people.on 'change', () ->
      AppLocalStorage.addCurrentPeople($(this).val())
      Search.form.submit()

    Search.tags.on 'change', () ->
      AppLocalStorage.addCurrentTags($(this).val())
      Search.form.submit()

    Search.orderBy.on 'change', () ->
      AppLocalStorage.setCurrentOrder($(this).val())
      Search.form.submit()

    Search.first.on 'click', ->
      Search.firstHidden.removeAttr('disabled')
      Search.form.submit()

    Search.previous.on 'click', ->
      Search.prevHidden.removeAttr('disabled')
      Search.form.submit()

    Search.next.on 'click', ->
      Search.nextHidden.removeAttr('disabled')
      Search.form.submit()

    Search.last.on 'click', ->
      Search.lastHidden.removeAttr('disabled')
      Search.form.submit()

    Search.favs.on 'change', ->
      if $(this).is(':checked')
        $(this).val('True')
      else
        $(this).val('False')
      AppLocalStorage.setFavoritesOnly($(this).is(':checked'))
      Search.form.submit()

    Search.form.submit (e) ->
      e.preventDefault()

      $.ajax
        url: '/images'
        type: 'GET'
        data: Search.form.serialize()
        success: (data) ->
          Search.firstHidden.attr('disabled', 'disabled')
          Search.nextHidden.attr('value', data.page + 1).attr('disabled', 'disabled')
          Search.prevHidden.attr('value', data.page - 1).attr('disabled', 'disabled')
          Search.lastHidden.attr('value', data.pages).attr('disabled', 'disabled')
          Search.currentPage.attr('value', data.page)

          if data.page > 1
            Search.first.removeAttr('disabled')
            Search.previous.removeAttr('disabled')
          else
            Search.first.attr('disabled', 'disabled')
            Search.previous.attr('disabled', 'disabled')

          if data.page == data.pages
            Search.next.attr('disabled', 'disabled')
            Search.last.attr('disabled', 'disabled')
          else
            Search.next.removeAttr('disabled')
            Search.last.removeAttr('disabled')

          $('#images').html data.template
          Search.activateFavoriteButton()
          Search.deleteImage()
          Search.viewPerson()

    Search.openImages.on 'click', ->
      images = []
      $images = $('.viewImages').find('.thumbnail');
      $images.each (index, image) ->
        $id = $(image).data('imageid')
        images.push($id)

      $.ajax
        url: '/api/images/open'
        method: 'POST'
        data:
          images: images
        success: (data) ->
          console.log data

  @activateFavoriteButton = ->
    $('.fav-img-button').each () ->
      $(this).on 'click', () ->
        self = $(this)
        $thumbnail = self.closest('.thumbnail')
        $span = self.find('span')

        imgId = $thumbnail.data('imageid')
        fav = $span.hasClass('fa-star')

        $.ajax
          url: '/api/image/favorite'
          method: 'POST'
          data:
            imgId: imgId
            favorite: fav
          success: (data) ->
            if data.result is 1
              $span.removeClass('fa-star-o').addClass('fa-star')
            else if data.result is 0
              $span.removeClass('fa-star').addClass('fa-star-o')

  @deleteImage = ->
    $('.delete-img-button').each ->
      $col = $(this).closest('.img-col')
      $(this).on 'click', ->
        Image.deleteImage($(this).closest('.thumbnail').data('imageid'), $col)
        $('#searchImages').submit()

  @viewPerson = ->
    $('.viewPerson').each ->
      $(this).on 'click', (e) ->
        personId = $(e.currentTarget).data('personid')
        name = $(e.currentTarget).data('name')
        Search.people.append($('<option selected/>').val(personId).text(name))
        Search.people.trigger('change')

  @editMultipleImages = ->
    $editMulti = $('input[name="editMultipleImages"]')
    $btnEditImages = $('button[name="btnEditMultipleImages"]')
    $btnDeleteImages = $('button[name="btnDeleteMultipleImages')
    $editGroup = $('.edit-images-group')

    $editMulti.on 'click', (e) ->
      checks = $('.multiSelect')

      if $(this).is(':checked')
        $editGroup.removeClass('hidden')
        checks.each (index, check) ->
          $(check).removeClass('hidden')
          $(check).on 'click', ->
            if $(check).find('span').hasClass('fa-square-o')
              $(this).find('span').removeClass('fa-square-o').addClass('fa-check-square-o')
            else
              $(this).find('span').addClass('fa-square-o').removeClass('fa-check-square-o')
      else
        $editGroup.addClass('hidden')
        checks.each (index, check) ->
          $(check).addClass('hidden').find('span').addClass('fa-square-o').removeClass('fa-check-square-o')
          AppLocalStorage.clearImagesToEdit()

    $btnEditImages.on 'click', ->
      checks = $('.multiSelect')
      toEdit = []
      checks.each (index, check) ->
        if $(this).find('span').hasClass('fa-check-square-o')
          imgId = $(check).closest('.thumbnail').data('imageid')
          toEdit.push(imgId)

      AppLocalStorage.setImagesToEdit(toEdit)

    $btnDeleteImages.on 'click', ->
      checks = $('.multiSelect')
      toDelete = []

      checks.each (index, check) ->
        if $(this).find('span').hasClass('fa-check-square-o')
          imgId = $(check).closest('.thumbnail').data('imageid')
          toDelete.push(imgId)

      i = 0;
      while i < toDelete.length
        Image.deleteImage(toDelete[i])
        i++

      $('#searchImages').submit()

$ ->
  Search.setUpForm()
