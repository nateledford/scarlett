$ ->
  $btn = $("#btnAddTag")
  $modal = $("#addTagModal")
  $modal.on "show.bs.modal", ->
    $("span#add-tag-error").addClass "hidden"
    txt = $(this).find("input[type=\"text\"]")
    txt.val ""

    $("#txtAddTag").keyup (event) ->
      if event.keyCode is 13
        $btn.click()

  $btn.on "click", ->
    tag = $modal.find("input[type=\"text\"]").val()
    $.ajax
      url: "/api/tags"
      type: "POST"
      data:
        tagName: tag

      success: (data) ->
        $('#txtAddTag').notify(data.message, if data.result is false then "warn" else "success")
        $modal.find("input[type=\"text\"]").val('').focus()