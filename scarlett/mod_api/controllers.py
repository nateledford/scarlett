import os

from flask import request, Blueprint, jsonify

import scarlett
from config import Config
from scarlett.data import session, Image
from scarlett.data.models.People import Person
from scarlett.data.models.Tags import Tag
from ..util.json import JsonSerialize, AjaxResult

api = Blueprint('api', __name__)


@api.route('/image/local', methods=['GET', 'POST'])
def image():
    if request.method == 'GET':
        img = Image.get_image(session, request.args.get('imageId'))
        return jsonify(AjaxResult(True, 'Image retrieved successfully', img.to_json()).to_json())

    if request.method == 'POST':
        file = request.files['img']
        t = request.form.getlist('tags')
        p = request.form.getlist('people')

        img = Image.create_image(session, scarlett.storage, file, t, p)

        return jsonify(AjaxResult(True, 'Image uploaded successfully!', img).to_json())


@api.route('/images/local', methods=['GET', 'POST'])
def images():
    if request.method == 'GET':
        return 'Huh'

    if request.method == 'POST':
        local_dir = request.form['dir']
        t = request.form.getlist('tags')
        p = request.form.getlist('people')

        result = Image.create_images(scarlett.storage, session, local_dir, t, p)

        return jsonify(AjaxResult(True, 'Images uploaded successfully', result).to_json())


@api.route('/image/remote', methods=['POST'])
def upload_remote_image():
    url = request.form.get('url')
    t = request.form.getlist('tags')
    p = request.form.getlist('people')

    img = Image.create_image_from_url(scarlett.storage, session, url, t, p)
    return jsonify(AjaxResult(True, 'Image uploaded successfully!', img).to_json())


@api.route('/image/open/<image_id>', methods=['POST'])
def open_image(image_id):
    i = Image.get_image(session, image_id)
    Image.open_images(session, [i.Image_Name])
    # TODO update client-side message
    return jsonify(AjaxResult(True, 'Opening image...').to_json())


@api.route('/images/open', methods=['POST'])
def open_images():
    Image.open_images(session, request.form.getlist('images[]'))

    return jsonify(AjaxResult(True, 'Opening images...').to_json())


@api.route('/image/tags', methods=['POST'])
def image_tags():
    if request.method == 'POST':
        img_id = request.args.get('imageId')
        t_id = request.args.get('tagId')
        action = request.args.get('action')

        if action == 'add':
            result = Image.add_tags(session, [img_id], [t_id])
        else:
            result = Image.remove_tag(session, img_id, t_id)

        if result:
            return jsonify(AjaxResult(True, 'Tags added to images successfully').to_json())
        else:
            return jsonify(AjaxResult(False, 'An error occurred').to_json())


@api.route('/images/tags', methods=['POST'])
def images_tags():
    if request.method == 'POST':
        img_ids = request.form.getlist('imageIds[]')
        tag_ids = request.form.getlist('tagIds[]')

        Image.add_tags(session, img_ids, tag_ids)

        return jsonify(AjaxResult(True, 'Tags attached to images successfully').to_json())


@api.route('/images/people', methods=['POST'])
def images_people():
    if request.method == 'POST':
        img_id = request.args.get('imageId')
        p_id = request.args.get('personId')
        action = request.args.get('action')

        if action == 'add':
            result = Image.add_person(session, img_id, p_id)
            return jsonify(AjaxResult(True, 'Person added to image successfully', result.to_json()).to_json())
        else:
            result = Image.remove_person(session, img_id, p_id)
            return AjaxResult(True, 'Person removed from image successfully', result.to_json()).to_json()


@api.route('/image/favorite', methods=['POST'])
def favorite_image():
    img_id = request.form.get('imgId')
    fav = request.form.get('favorite')

    if fav == 'true':
        fav = 0
    elif fav == 'false':
        fav = 1

    img = session.query(Image).get(int(img_id))
    img.Favorite = fav
    session.commit()

    return jsonify({'result': fav})


@api.route('/tag/<tag_id>', methods=['GET'])
def tag(tag_id):
    t = Tag.get_tag(session, tag_id)
    return jsonify(AjaxResult(True, 'Tag retrieved successfully', t.to_json()).to_json())


@api.route('/tags', methods=['GET', 'POST'])
def tags():
    if request.method == 'GET':
        q = request.args.get('q')

        if q is None:
            result = Tag.get_tags(session)
        else:
            result = Tag.search_tags(session, q)

        j = []
        for t in result:
            j.append(t.to_json())

        return jsonify(AjaxResult(True, 'Tags retrieved successfully', j).to_json())

    if request.method == 'POST':
        result = Tag.create_tag(session, request.form['tagName'])

        if not result:
            return jsonify(AjaxResult(False, 'Tag already exists in database').to_json())
        else:
            return jsonify(AjaxResult(True, 'Tag created successfully').to_json())


@api.route('/people', methods=['GET', 'POST'])
def people():
    if request.method == 'GET':
        q = request.args.get('q')

        if q is None:
            p = session.query(Person).order_by(Person.Name).all()
        else:
            p = session.query(Person).order_by(Person.Name).filter(Person.Name.like('%' + q + '%'))
        return jsonify({'people': JsonSerialize.convert_list_to_json(p)})

    if request.method == 'POST':
        directory = request.form['directory']
        result = AjaxResult().to_json()

        new_count = 0
        folders = os.listdir(directory)
        for folder in folders:
            # Check if person already exists in database
            exists = session.query(Person).filter(Person.Name == folder).count()

            # If they don't exist, add them to database
            if exists == 0:
                p = Person()
                p.Name = folder
                session.add(p)
                session.commit()
                new_count += 1

            if new_count == 0:
                result = {'message': 'No new people were added to the database', 'count': new_count}
            elif new_count == 1:
                result = {'message': 'One new person was added to the database', 'count': new_count}
            else:
                result = {'message': '{} new people were added to the database'.format(new_count), 'count': new_count}

        return jsonify(result)


@api.route('/person', methods=['POST'])
def create_person():
    if request.method == 'POST':
        p = Person()
        p.Name = request.form['name']
        session.add(p)
        session.commit()
        return jsonify({'result': True, 'person': p.to_json()})


@api.route('/person/<person_id>', methods=['GET', 'DELETE', 'PUT'])
def person(person_id):
    if request.method == 'GET':
        if person_id is not None:
            p = session.query(Person).get(person_id)
            return jsonify(AjaxResult(True, '{0} retrieved successfully'.format(p.Name), p.to_json()).to_json())
        else:
            return jsonify(AjaxResult(False, 'No person was found'))

    if request.method == 'DELETE':
        p = session.query(Person).get(person_id)
        session.delete(p)
        session.commit()
        return jsonify({'result': 'Person deleted successfully'})

    if request.method == 'PUT':
        p = session.query(Person).get(person_id)

        if request.form['name'] is not None and request.form['name'] != '':
            p.Name = request.form['name']

        if request.form['facebook'] is not None and request.form['facebook'] != '':
            p.FacebookUserName = request.form['facebook']

        if request.form['instagram'] is not None and request.form['instagram'] != '':
            p.InstagramUserName = request.form['instagram']

        if request.form['twitter'] is not None and request.form['twitter'] != '':
            p.TwitterUserName = request.form['twitter']

        session.commit()

        return jsonify(AjaxResult(True, 'Person updated successfully!').to_json())


@api.route('/settings', methods=['GET', 'POST'])
def settings():
    c = Config()

    if request.method == 'GET':
        j = c.to_json()

        return jsonify(AjaxResult(True, 'Settings retrieved successfully', j).to_json())

    if request.method == 'POST':
        c.STORAGE_CONTAINER = request.form['container']
        c.DB_PATH = request.form['db']
        c.PORT = int(request.form['port'])
        c.DEBUG = True if request.form['debug'] == 'on' else False
        c.ASSETS_DEBUG = True if request.form['assets'] == 'on' else False

        c.save_config()

        return jsonify(AjaxResult(True, 'Settings updated successfully').to_json())


@api.route('/stats/image/count', methods=['GET'])
def get_image_count():
    count = session.query(Image).count()
    return jsonify(AjaxResult(True, 'Number of images retrieved', count).to_json())


# SOURCE: http://www.davidadamojr.com/handling-cors-requests-in-flask-restful-apis/
@api.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response
