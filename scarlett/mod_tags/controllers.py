from flask import Blueprint, render_template

from scarlett.data import session
from scarlett.data.models.Tags import Tag

tags = Blueprint('tags', __name__)


@tags.route('/')
def index():
    t = session.query(Tag).order_by(Tag.TagName).all()
    return render_template('tags/tags.jade', title='Tags', tags=t)
